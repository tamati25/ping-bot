#!/usr/bin/env python3.5
#-*- encoding: utf-8

import os
import datetime
import asyncio
import discord
try:
    import cPickle as pickle
except Exception as e:
    import pickle

# TODO cleaning, first draft here
client = discord.Client()

# TODO put this in a file.
gym_zones = {
    "Nord-Ouest": [ "achauchatarc", "arbredumerit", "auxvictimesd", "blasondu143", "eglisereform", "fresquedelap", "horlogedesab", "lesnymphesdu", "lithortue", "losninos",
     "memorialauge", "memorialauxg", "Memorialrkel", "museepaulbel", "parcourseque", "statuedesfem", "theatredelou"], 
    "EST": [ "airedejeux", "cabanedebois", "fresqueenfan", "frontonchiqu", "groupescolai", "immeubleogic", "lepointdujou", "placedelapor", "squarecoquoi", "stadegeoandr",
    "stadejeanbou", "stadepierred", "statuekapfer", "tourtf1", "tramvaldeseine" ],
    "Parc Ile St Germ.": [ "airedejeuxet", "cabanedebois", "jeanmoulin", "koskunII", "sculptureaux", "sculpturedeb", "sculpturesab", "statuekapfer"],
    "Nord-Est": ["bibliotheque", "bustedevicto", "centremaitre", "eglisereform", "lafresque", "losninos", "monumentsaux", "piscinemolit", "stadegeoandr" ],
    "Ouest": [ "arbredumerit", "blasondu143", "champignonto", "chateaudecor", "fontaine", "jacqueslipch", "lachaumierev", "lafortunedeb", "lapubcestnul", "leposeidonde", 
    "lesnymphesdu", "nosnos", "parcoursindu", "statuedesfem", "stele", "steleyvesker", "unindiendans" ],
    "Glacieres": [ "boulognelimm", "envoleeaviai", "fresqueenfan", "parcdesglaci", "piscineboulo", "porchefleuri", "toughcookiev", "vieillardala" ],
    "Parc Billancourt-Trapeze": ["boulognelimm", "girafe", "maisonsaintf", "plandutrapez", "porterenault", "stele", "steleyvesker"],
    "Centre-Marcel Sembat": [ "carrebellefe", "centremaitre", "eglisereform", "fontaine", "immeubleogic", "jacqueslipch", "lafortunedeb", "lafresque", "leposeidonde",
    "metromarcels", "mosaiqueandr", "piscineboulo", "statuedesfem", "unindiendans"]
}

def get_token():
    token = None
    with open(".token", "r") as f:
        token = f.read()
    return token


@client.event
async def on_ready():
    print("on_ready start")
    # TODO

    return


@client.event
async def on_message(message):
    words = message.content.split() 
    if (words[0] == '<@' + str(client.user.id) + '>'):
        if len(words) < 2:
            help_msg = """
            Utilisation:
              @ping-bot#9527 <cmd> <arene> <arene> <arene>
              <jour> <heuredebut>-<heurefin> <heuredebut>-<heurefin>
              <jour> <heuredebut>-<heurefin>
            ...

            jour est le jour de la semaine en toutes lettres (lundi, mardi, mercredi...)
            Commandes :
              ARENE Liste les arenes connues
              ADD   S'inscrit aux alertes sur l'arene aux heures definies
              DEL   Se desinscrit des alertes sur l'arene aux heures definies
            
            Exemple:
              @ping-bot#9527 ADD stadepierred tourtf1
              lundi 0930-1800
              mardi 0930-1800
              mercredi 0930-1800
              jeudi 0930-1800
              vendredi 0930-1800
            """
            await client.send_message(message.channel, help_msg)
            return

        if words[1] == 'ARENE':
            msg = "```Liste des arenes reconnues :\n"
            lines = [line.strip() for line in open("gym_with_coords", "r")] 
            for line in lines:
                msg += line.split(";")[0] + "\n"

            msg += "```"
            await client.send_message(message.channel, msg)
            return

        if words[1] == 'ADD' or words[1] == 'DEL':
            first_line = message.content.split("\n")[0]
            if len(words) < 3:
                print("Erreur dans la commande, veuillez reessayer")

            gyms = first_line.split()[2:]
            s = ' '.join(word for word in gyms)
            if s in gym_zones:
                gyms = gym_zones[s]

            message_to_send = ""
            for gym in gyms:
                try:
                    lines = message.content.split("\n")[1:]
                    user = message.author
                    add = (words[1] == 'ADD')
                    await subscribe(message.channel, gym, lines, user, add)
                    message_to_send += "[{0}] OK\n".format(gym)
                except Exception as e:
                    print(str(e))
                    message_to_send += "[{0}] Une erreur est survenue :(, contacter @tama#9741 pour plus d'informations\n".format(gym)
                    
            await client.send_message(message.channel, message_to_send)
            return

        # Unknown command
        await client.send_message(message.channel, "commande {0} inconnue".format(words[1]))


async def subscribe(channel, gym, data, user, add):
    filename = "subscribe_{0}".format(channel.server.id)
    if not os.path.exists(filename):
        cur_data = {}
    else:
        cur_data = load(filename)

    error_msg = ""
    valid_days = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

    if add is True:
        if user.name not in cur_data:
            cur_data[user.name] = {}
            
        if gym not in cur_data:
            cur_data[user.name][gym] = {}
            
    for line in data:
        words = line.split()
        day = words[0].lower()
        if day not in valid_days:
            error_msg += "Ligne invalide : {0}\nLe jour de la semaine n'est pas connu\n".format(line)
            continue

        if add is True:
            if day not in cur_data[user.name][gym]:
                cur_data[user.name][gym][day] = []

        for word in words[1:]:
            start = word.split("-")[0]
            end = word.split("-")[1]

            if len(start) != 4 or len(end) != 4:
                error_msg +=  "Ligne invalide : {0}\nUne heure est au mauvais format (sur 4 chiffres, de 0000 a 2359)\n".format(line)
                continue

            try:
                istart = int(start)
                iend = int(end)
            except Exception as e:
                error_msg += "Ligne invalide : {0}\nHeure invalide\n".format(line)
                continue

            if istart < 0 or iend < 0 or istart > 2359 or iend > 2359 or istart % 100 > 59 or iend % 100 > 59:
                error_msg += "Ligne invalide : {0}\nUne heure fournie est invalide (entre 0000 et 2359)\n".format(line)
                continue

            if iend <= istart:
                error_msg += "Ligne invalide : {0}\nL'heure de fin est avant l'heure de debut\n".format(line)
                continue

            # Check ok, add to list
            if add is True:
                print("ADD [{0}] {1} : {2} -> {3} ({4})".format(gym, user.name, istart, iend, day))
                cur_data[user.name][gym][day].append((istart, iend))
            else:
                print("DEL [{0}] {1} : {2} -> {3} ({4})".format(gym, user.name, istart, iend, day))
                new_list = []
                for saved in cur_data[user.name][gym][day]:
                    if istart >= saved[0] and iend <= saved[1]:
                        # totally included
                        new_list.append((saved[0], istart))
                        new_list.append((iend, saved[1]))
                    elif istart <= saved[0] and iend >= saved[0] and iend <= saved[1]:
                        # partially included (shave off start)
                        new_list.append((iend, saved[1]))
                    elif istart <= saved[1] and istart >= saved[0] and iend >= saved[1]:
                        # partially included (shave off end)
                        new_list.append((saved[0], istart))
                    elif iend < saved[0] or istart > saved[1]:
                        # not included, save the old value
                        new_list.append((saved[0], saved[1]))
                    else:
                        # totally destroyed, do not save old value
                        pass
                    
                cur_data[user.name][gym][day] = new_list

    if error_msg != "":
        await client.send_message(channel, error_msg)
    save(filename, cur_data)

async def run(token):
    await client.login(token)
    await client.connect()


@client.event
async def on_channel_create(channel):
    # Get informations
    filename = "subscribe_{0}".format(channel.server.id)
    if not os.path.exists(filename):
        cur_data = {}
    else:
        cur_data = load(filename)

    # Day of week
    dow = datetime.datetime.today().weekday()
    daylist = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
    day = daylist[dow]

    # Gym
    gym = channel.name.split("-")[1]
    raidend = channel.name.split("-")[2]

    # Ugly, but should do the job for now
    raidendnum = extract_digit(raidend)
    if len(raidendnum) < 4:
        raidendnum += "00"
    iraidendnum = int(raidendnum)
    iraidstartnum = iraidendnum - 45
    if iraidstartnum % 100 >= 55:
        iraidstartnum -= 40

    print("{0} : {1}-{2}".format(gym, iraidstartnum, iraidendnum))

    # Final list of players to ping
    ping = []
    for u in cur_data:
        if gym not in cur_data[u]:
            continue

        if day not in cur_data[u][gym]:
            continue

        for r in cur_data[u][gym][day]:
            print("{0}-{1}".format(r[0], r[1]))
            if iraidstartnum >= r[0] and iraidendnum <= r[1]:
                # Included (inside)
                print("A")
                ping.append(u)
                continue

            if iraidstartnum <= r[0] and iraidendnum >= r[0] and iraidendnum <= r[1]:
                # Included (start)
                print("B")
                ping.append(u)
                continue

            if iraidstartnum >= r[0] and iraidstartnum <= r[1] and iraidendnum >= r[1]:
                # Included (end)
                print("C")
                ping.append(u)
                continue

            if iraidstartnum <= r[0] and iraidendnum >= r[1]:
                # Included (outside)
                print("D")
                ping.append(u)
                continue

    print(ping)
    final_message = ""

    # Nobody is subscribed, do nothing
    if len(ping) == 0:
        return

    for user_to_ping in ping:
        m = discord.utils.get(channel.server.members, name=user_to_ping)
        final_message += m.mention + " "

    final_message += "\n---\n"
    final_message += "Ce bot mentionne automatiquement les personnes s'etant declarees interesses au moment ou se deroule le raid. Pour plus d'informations, envoyer @ping-bot#9725\n"
    final_message += "En cas de bug, mentionner @tama#9741 !\n"
    final_message += "Code source : [ca arrive]"
    await client.send_message(channel, final_message)

        
# --
# Save & load data
# --
def save(filename, data):
    pickle.dump(data, open(filename, "wb"))

def load(filename):
    return pickle.load(open(filename, "rb"))

# --
# Utils
# --
def extract_digit(s):
    return ''.join(c for c in s if c.isdigit())

if __name__ == '__main__':
    token = get_token()
    if token is not None:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run(token))

